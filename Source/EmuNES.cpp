// EmuNES.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	unsigned char x = 0x80;
	std::cout << (unsigned int)x << std::endl;
	unsigned short y = 0x17F;
	std::cout << (unsigned int)y << std::endl;
	x = static_cast<unsigned char>(y);
	std::cout << (unsigned int)x;

	return 0;
}

