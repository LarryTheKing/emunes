#include "Types.h"
#include "Opcodes.h"

namespace EmuNES
{
#define FLAG_NEGATIVE	0x80	// N // Set if bit 7 of the accumulator is set.
#define FLAG_OVERFLOW	0x40	// V //
#define FLAG_BRK		0x10	// B // Set if an interrupt caused by a BRK
#define FLAG_DECIMAL	0x08	// D // Set if decimal mode active (nonfunctional)
#define FLAG_IRQ_DIS	0x04	// I // Set if maskable interrupts are disabled.
#define FLAG_ZERO		0x02	// Z // Set if the result of the last operation was zero
#define FLAG_CARRY		0x01	// C // Set if the add produced a carry, or if the subtraction	produced a borrow. Also holds bits after a logical shift.

	struct Operation
	{
		union
		{
			uint32	_RAW;	// An operation and its arguments will never exceed 32 bytes in size
			struct 
			{
				// 00 11 22 33		An operation can have up to two argumenets,
				// OP XX XX XX		these argumennts may contain at most ONE short 

				uint8	Op;	// The 8bit operation
				
				union		// The argumenets take up the remaining 3 bytes
				{
					uint16	s1;	// OP S1 S1 XX
					struct
					{
						uint8 c1;	// OP C1 XX XX
						union
						{
							uint16 s2;	// OP C1 S1 S1
							struct
							{
								uint8 c2;	// OP C1 C2 XX
								uint8 c3;	// OP S1 S1 C3
							};				
						};
					};
				};
			};
		};
	};

	class Interpreter
	{
		struct Reg
		{
			union
			{
				uint64 _RAW;
				struct{
					uint8	A;	// Accumulator
					uint8	X;	// X Index
					uint8	Y;	// Y Index
					uint8	S;	// Stack Pointer
					uint8	P;	// Processor Status	: Stores the 'FLAGS'
					// uint8 _PAD;
					uint16	PC;	// Program Count
				};
			};
		};

		uint8 * mem;


	public:
		void Reset();
		void Run(char * pOps, uint32 cycles);

	public:


	private:

	};
}