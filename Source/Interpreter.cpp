#include "Interpreter.h"

namespace EmuNES
{
	void Interpreter::Run(char * pOps, uint32 cycles)
	{
		static Reg reg;
		reg._RAW = 0;

		static uint32 c = 0;	// The cycle count
		//static uint32 i = 0;	// The index for the current operation

		static Operation op;	// Holds the current operation and its arguments
		op._RAW = 0;

#define CYCLE(N)	(c += N)
#define INC(N)		(reg.PC += N)

#define SET_NEGATIVE(V)	(reg.P = ( (V & 0x80)	? reg.P | FLAG_NEGATIVE : reg.P & (~FLAG_NEGATIVE))) // Sets the negative flag if bit 7 is set, otherwise clears it.
#define SET_ZERO(V)		(reg.P = ( (V & 0xFF)	? reg.P & (~FLAG_ZERO) : reg.P | FLAG_ZERO)) // Sets the Zero Flag if the Operand is 0x00, otherwise clears it. (Only observes the last 8bits)
#define SET_CARRY(V)	(reg.P = ((V & (~0xFF))	? reg.P | FLAG_CARRY : reg.P & (~FLAG_CARRY)))	// Sets the carry flag if any bit NOT 0-7 is set, otherwise clears it.
#define SET_CARRY_7(V)	(reg.P = ( (V & 0x80)	? reg.P | FLAG_CARRY : reg.P & (~FLAG_CARRY)))  // Sets the carry flag if bit 7 is set, otherwise clears it.
#define SET_CARRY_0(V)	(reg.P = ( (V & 0x01)	? reg.P | FLAG_CARRY : reg.P & (~FLAG_CARRY)))  // Sets the carry flag if bit 0 is set, otherwise clears it.
#define SET_OVERFLOW(V) (reg.P = ( (reg.A & 0x80) != (V & 0x80) ? reg.P | FLAG_OVERFLOW : reg.P & (~FLAG_OVERFLOW))) // Sets the overflow flag if bit 7 is different than in the accumulator, otherwise clears it.
#define SET_DECIMAL(V)	(reg.P = ( (V)			? reg.P | FLAG_DECIMAL : reg.P & (~FLAG_DECIMAL)))	// Sets the decimal flag if op is not 0, otherwise clears it.
#define SET_IRQ(V)		(reg.P = ( (V)			? reg.P | FLAG_IRQ_DIS : reg.P & (~FLAG_IRQ_DIS)))	// Sets the interupt disable flag if op is not 0, otherwise clears it.

#define GET_CARRY()			(reg.P & FLAG_CARRY)	// Returns 0x01 if the carry flag is set, otherwise returns 0x00.
#define GET_ZERO()			(reg.P & FLAG_ZERO)		// Returns FLAG_ZERO if the zero flag is set, otherwise returns 0x00.
#define GET_NEGATIVE()		(reg.P & FLAG_NEGATIVE) // Returns FLAG_NEGATIVE if the negative flag is set, otherwise returns 0x00.
#define GET_OVERFLOW()		(reg.P & FLAG_OVERFLOW) // Returns FLAG_OVERFLOW if the overflow flag is set, otherwise returns 0x00.

#define GET_ZP(U)	(mem[U])	// Gets a byte value in the zero page
#define GET_MEM(U)	(mem[U])	// Gets a byte value in memory
#define GET_MEM_S(U) (*reinterpret_cast<uint16*>(mem + U))	// Gets a short value from memory
#define SET_ZP(U, V)	(mem[U] = V)	// Sets a byte value in the zero page
#define SET_MEM(U, V)	(mem[U] = V)	// Sets a byte value in memory

#define PUSH_STACK(V)	(mem[0x1FF - (reg.S++)] = V)	// Pushes a byte onto the stack
#define PULL_STACK()	(mem[0x1FF - (reg.S--)])		// Pulls a byte from the stack

#define OP_ROL(V)		((V << 1) | GET_CARRY()) // Rotates the byte to the left using carry
#define OP_ROR(V)		((V >> 1) | (GET_CARRY() << 7)) // Rotates the byte to the right using carry
#define OP_ROR_S(V)		(((V & 0x01) ? 0x100 : 0x00) | (V >> 1) | (GET_CARRY() << 7)); // Rotates the byte to the right using carry. Stores REAL rolled bit in bit 8.
#define OP_CMP(R, V)	(reg.P |= (R < V ? FLAG_NEGATIVE : (R == V ? FLAG_ZERO | FLAG_CARRY : FLAG_CARRY)))	// Compares a value to the value of a register. Stores result by setting flags.
		
		
		while (c < cycles)
		{
			op._RAW = *reinterpret_cast<uint32 *>(pOps + reg.PC);

			switch (op.Op)
			{
			case(BRK) :	// BRK ... unsused?
			case(NOP) :	// NOP
				INC(1); CYCLE(2);
				break;
			#pragma region Storage
			#pragma region LDA // Load into A
			case(LDA_IMM) : {	// LDA #O : Immediate
				SET_NEGATIVE(op.c1); SET_ZERO(op.c1);
				reg.A = op.c1;
				CYCLE(2); INC(2);
				} break;
			case(LDA_ZPG) : {	// LDA O : Zero Page
				uint8 o = GET_ZP(op.c1);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(3); INC(2);
				} break;
			case(LDA_ZPGX) : {	// LDA O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(4); INC(2);
				} break;
			case(LDA_ABS) : {	// LDA O : Absolute
				uint8 o = GET_MEM(op.s1);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(4); INC(3);
				} break;
			case(LDA_ABSX) : {	// LDA O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(4); INC(3);
				} break;
			case(LDA_ABSY) : {	// LDA O, Y : Absolute
				uint8 o = GET_MEM(op.s1 + reg.Y);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(4); INC(3);
				} break;
			case(LDA_INDX) : {	// LDA O, X : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.X));
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(6); INC(2);
				} break;
			case(LDA_INDY) : {	// LDA O, Y : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.Y));
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(6); INC(2);
				} break;
#pragma endregion
			#pragma region LDX // Load into X
			case(LDX_IMM) : {	// LDX #O : Immediate
				SET_NEGATIVE(op.c1); SET_ZERO(op.c1);
				reg.X = op.c1;
				INC(2); CYCLE(2);
				} break;
			case(LDX_ZPG) : {	// LDX O : Zero Page
				uint8 o = GET_ZP(op.c1);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.X = o;
				INC(2); CYCLE(3);
				} break;
			case(LDX_ZPGY) : {	// LDX O, y : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.Y);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.X = o;
				INC(2); CYCLE(4);
				} break;
			case(LDX_ABS) : {	// LDX O : Absolute
				uint8 o = GET_MEM(op.s1);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.X = o;
				INC(3); CYCLE(4);
				} break;
			case(LDX_ABSY) : {	// LDX O, Y : Absolute
				uint8 o = GET_MEM(op.s1 + reg.Y);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.X = o;
				INC(3); CYCLE(4);
				} break;
#pragma endregion
			#pragma region LDY // Load into Y
			case(LDY_IMM) : {	// LDY #O : Immediate
				SET_NEGATIVE(op.c1); SET_ZERO(op.c1);
				reg.Y = op.c1;
				INC(2); CYCLE(2);
				} break;
			case(LDY_ZPG) : {	// LDY O : Zero Page
				uint8 o = GET_ZP(op.c1);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.Y = o;
				INC(2); CYCLE(3);
				} break;
			case(LDY_ZPGX) : {	// LDY O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.X = o;
				INC(2); CYCLE(4);
				} break;
			case(LDY_ABS) : {	// LDY O : Absolute
				uint8 o = GET_MEM(op.s1);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.Y = o;
				INC(3); CYCLE(4);
				} break;
			case(LDY_ABSX) : {	// LDY O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X);
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.X = o;
				INC(3); CYCLE(4);
				} break;
#pragma endregion
			#pragma region STA // Store A in mem
			case(STA_ZPG) : {	// STA O : Zero Page
				SET_ZP(op.c1, reg.A);
				INC(2); CYCLE(3);
				} break;
			case(STA_ZPGX) : {	// STA O, X : Zero Page
				SET_ZP(op.c1 + reg.X, reg.A);
				INC(2); CYCLE(4);
				} break;
			case(STA_ABS) : {	// STA O : Absolute
				SET_MEM(op.s1, reg.A);
				INC(3); CYCLE(4);
				} break;
			case(STA_ABSX) : {	// STA O, X : Absolute
				SET_MEM(op.s1 + reg.X, reg.A);
				INC(3); CYCLE(4);
				} break;
			case(STA_ABSY) : {	// STA O, Y : Absolute
				SET_MEM(op.s1 + reg.Y, reg.A);
				INC(3); CYCLE(4);
				} break;
			case(STA_INDX) : {	// STA O, X : Indirect
				SET_MEM(GET_MEM(op.s1 + reg.X), reg.A);
				INC(2); CYCLE(6);
				} break;
			case(STA_INDY) : {	// STA O, Y : Indirect
				SET_MEM(GET_MEM(op.s1 + reg.Y), reg.A);
				INC(2); CYCLE(6);
				} break;
#pragma endregion
			#pragma region STX // Store X in mem
			case(STX_ZPG) : {	// STX O : Zero Page
				SET_ZP(op.c1, reg.X);
				INC(2); CYCLE(3);
				} break;
			case(STX_ZPGY) : {	// STX O, Y : Zero Page
				SET_ZP(op.c1 + reg.Y, reg.X);
				INC(2); CYCLE(4);
				} break;
			case(STX_ABS) : {	// STX O : Absolute
				SET_MEM(op.s1, reg.X);
				INC(3); CYCLE(4);
				} break;
			#pragma endregion
			#pragma region STY // Store Y in mem
			case(STY_ZPG) : {	// STY O : Zero Page
				SET_ZP(op.c1, reg.Y);
				INC(2); CYCLE(3);
				} break;
			case(STY_ZPGX) : {	// STY O, X : Zero Page
				SET_ZP(op.c1 + reg.X, reg.Y);
				INC(2); CYCLE(4);
				} break;
			case(STY_ABS) : {	// STY O : Absolute
				SET_MEM(op.s1, reg.Y);
				INC(3); CYCLE(4);
				} break;
			#pragma endregion
			#pragma region T // Transfer between registers
			case(TAX) : {	// TAX
				SET_NEGATIVE(reg.A); SET_ZERO(reg.A);
				reg.X = reg.A;
				INC(1); CYCLE(2);
				} break;
			case(TAY) : {	// TAY
				SET_NEGATIVE(reg.A); SET_ZERO(reg.A);
				reg.Y = reg.A;
				INC(1); CYCLE(2);
				} break;
			case(TSX) : {	// TSX
				SET_NEGATIVE(reg.S); SET_ZERO(reg.S);
				reg.X = reg.S;
				INC(1); CYCLE(2);
				} break;
			case(TXA) : {	// TXA
				SET_NEGATIVE(reg.X); SET_ZERO(reg.X);
				reg.A = reg.X;
				INC(1); CYCLE(2);
				} break;
			case(TXS) : {	// TXS
				SET_NEGATIVE(reg.X); SET_ZERO(reg.X);
				reg.S = reg.X;
				INC(1); CYCLE(2);
				} break;
			case(TYA) : {	// TYA
				SET_NEGATIVE(reg.Y); SET_ZERO(reg.Y);
				reg.A = reg.X;
				INC(1); CYCLE(2);
				} break;
			#pragma endregion
			#pragma endregion
			#pragma region Math
			#pragma region ADC // Add to A with carry
			case(ADC_IMM) : {	// ADC #O : Immediate
				uint16 s = reg.A + op.c1 + GET_CARRY();
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(2); CYCLE(2);
				} break;
			case(ADC_ZPG) : {	// ADC O : Zero Page
				uint8 o = GET_ZP(op.c1);
				uint16 s = reg.A + o + GET_CARRY();
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(2); CYCLE(3);
				} break;
			case(ADC_ZPGX) : {	// ADC O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X);
				uint16 s = reg.A + o + GET_CARRY();
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(2); CYCLE(4);
				} break;
			case(ADC_ABS) : {	// ADC O : Absolute
				uint8 o = GET_MEM(op.s1);
				uint16 s = reg.A + o + GET_CARRY();
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(4);
				} break;
			case(ADC_ABSX) : {	// ADC O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X);
				uint16 s = reg.A + o + GET_CARRY();
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(4);
				} break;
			case(ADC_ABSY) : {	// ADC O, Y : Absolute
				uint8 o = GET_MEM(op.s1 + reg.Y);
				uint16 s = reg.A + o + GET_CARRY();
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(4);
				} break;
			case(ADC_INDX) : {	// ADC O, X : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.X));
				uint16 s = reg.A + o + GET_CARRY();
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(6);
				} break;
			case(ADC_INDY) : {	// ADC O, Y : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.Y));
				uint16 s = reg.A + o + GET_CARRY();
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(5);
				} break;
			#pragma endregion
			#pragma region SBC // Subtract from A with carry
			case(SBC_IMM) : {	// SBC #O : Immediate
				uint16 s = reg.A - op.c1 - (~ GET_CARRY());
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(2); CYCLE(2);
				} break;
			case(SBC_ZPG) : {	// SBC O : Zero Page
				uint8 o = GET_ZP(op.c1);
				uint16 s = reg.A - o - (~GET_CARRY());
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(2); CYCLE(3);
				} break;
			case(SBC_ZPGX) : {	// SBC O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X);
				uint16 s = reg.A - o - (~GET_CARRY());
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(2); CYCLE(4);
				} break;
			case(SBC_ABS) : {	// SBC O : Absolute
				uint8 o = GET_MEM(op.s1);
				uint16 s = reg.A - o - (~GET_CARRY());
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(4);
				} break;
			case(SBC_ABSX) : {	// SBC O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X);
				uint16 s = reg.A - o - (~GET_CARRY());
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(4);
				} break;
			case(SBC_ABSY) : {	// SBC O, Y : Absolute
				uint8 o = GET_MEM(op.s1 + reg.Y);
				uint16 s = reg.A - o - (~GET_CARRY());
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(4);
				} break;
			case(SBC_INDX) : {	// SBC O, X : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.X));
				uint16 s = reg.A - o - (~GET_CARRY());
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(6);
				} break;
			case(SBC_INDY) : {	// SBC O, Y : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.Y));
				uint16 s = reg.A - o - (~GET_CARRY());
				SET_CARRY(s); SET_NEGATIVE(s);  SET_ZERO(s); SET_OVERFLOW(s);
				reg.A = s & 0xFF;
				INC(3); CYCLE(5);
				} break;
#pragma endregion
			#pragma region DE // Decrement memory or register
			case(DEC_ZPG) : {	// DEC O : Zero Page
				uint8 o = --GET_ZP(op.c1);
				SET_NEGATIVE(o); SET_ZERO(o);
				INC(2); CYCLE(5);
				} break;
			case(DEC_ZPGX) : {	// DEC O, X : Zero Page
				uint8 o = --GET_ZP(op.c1 + reg.X);
				SET_NEGATIVE(o); SET_ZERO(o);
				INC(2); CYCLE(6);
				} break;
			case(DEC_ABS) : {	// DEC O : Absolute
				uint8 o = --GET_MEM(op.s1);
				SET_NEGATIVE(o); SET_ZERO(o);
				INC(3); CYCLE(6);
				} break;
			case(DEC_ABSX) : {	// DEC O, X : Absolute
				uint8 o = --GET_MEM(op.s1 + reg.X);
				SET_NEGATIVE(o); SET_ZERO(o);
				INC(2); CYCLE(7);
				} break;
			case(DEX) : {	// DEX 
				reg.X--;
				SET_NEGATIVE(reg.X); SET_ZERO(reg.X);
				INC(1); CYCLE(2);
				} break;
			case(DEY) : {	// DEY
				reg.Y--;
				SET_NEGATIVE(reg.Y); SET_ZERO(reg.Y);
				INC(1); CYCLE(2);
				} break;
			#pragma endregion
			#pragma region IN // Increment memory or register
			case(INC_ZPG) : {	// INC O : Zero Page
				uint8 o = ++GET_ZP(op.c1);
				SET_NEGATIVE(o); SET_ZERO(o);
				INC(2); CYCLE(5);
				} break;
			case(INC_ZPGX) : {	// INC O, X : Zero Page
				uint8 o = ++GET_ZP(op.c1 + reg.X);
				SET_NEGATIVE(o); SET_ZERO(o);
				INC(2); CYCLE(6);
				} break;
			case(INC_ABS) : {	// INC O : Absolute
				uint8 o = ++GET_MEM(op.s1);
				SET_NEGATIVE(o); SET_ZERO(o);
				INC(3); CYCLE(6);
				} break;
			case(INC_ABSX) : {	// INC O, X : Absolute
				uint8 o = ++GET_MEM(op.s1 + reg.X);
				SET_NEGATIVE(o); SET_ZERO(o);
				INC(2); CYCLE(7);
				} break;
			case(INX) : {	// INX 
				++reg.X;
				SET_NEGATIVE(reg.X); SET_ZERO(reg.X);
				INC(1); CYCLE(2);
				} break;
			case(INY) : {	// INY
				++reg.Y;
				SET_NEGATIVE(reg.Y); SET_ZERO(reg.Y);
				INC(1); CYCLE(2);
				} break;
			#pragma endregion
			#pragma endregion
			#pragma region Bitwise
			#pragma region AND // Perform the bitwise AND operation on A
			case(AND_IMM) : {	// AND #O : Immediate
				op.c1 &= reg.A;
				SET_NEGATIVE(op.c1); SET_ZERO(op.c1);
				reg.A = op.c1;
				CYCLE(2); INC(2);
				} break;
			case(AND_ZPG) : {	// AND O : Zero Page
				uint8 o = GET_ZP(op.c1) & reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(3); INC(2);
				} break;
			case(AND_ZPGX) : {	// AND O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X) & reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(4); INC(2);
				} break;
			case(AND_ABS) : {	// AND O : Absolute
				uint8 o = GET_MEM(op.s1) & reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(4); INC(3);
				} break;
			case(AND_ABSX) : {	// AND O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X) & reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(4); INC(3);
				} break;
			case(AND_ABSY) : {	// AND O, Y : Absolute
				uint8 o = GET_MEM(op.s1 + reg.Y) & reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(4); INC(3);
				} break;
			case(AND_INDX) : {	// AND O, X : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.X)) & reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(6); INC(2);
				} break;
			case(AND_INDY) : {	// AND O, Y : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.Y)) & reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				CYCLE(5); INC(2);
				} break;
			#pragma endregion
			#pragma region ORA // Perform the bitwise OR operation on A
			case(ORA_IMM) : {	// ORA #O : Immediate
				op.c1 |= reg.A;
				SET_NEGATIVE(op.c1); SET_ZERO(op.c1);
				reg.A = op.c1;
				INC(2); CYCLE(2);
				} break;
			case(ORA_ZPG) : {	// ORA O : Zero Page
				uint8 o = GET_ZP(op.c1) | reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(2); CYCLE(3);
				} break;
			case(ORA_ZPGX) : {	// ORA O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X) | reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(2); CYCLE(4);
				} break;
			case(ORA_ABS) : {	// ORA O : Absolute
				uint8 o = GET_MEM(op.s1) | reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(3); CYCLE(4);
				} break;
			case(ORA_ABSX) : {	// ORA O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X) | reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(3); CYCLE(4);
				} break;
			case(ORA_ABSY) : {	// ORA O, Y : Absolute
				uint8 o = GET_MEM(op.s1 + reg.Y) | reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(3); CYCLE(4);
				} break;
			case(ORA_INDX) : {	// ORA O, X : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.X)) | reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(2); CYCLE(6);
				} break;
			case(ORA_INDY) : {	// ORA O, Y : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.Y)) | reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(2); CYCLE(5);
				} break;
#pragma endregion
			#pragma region EOR // Perform the bitwise XOR operation on A
			case(EOR_IMM) : {	// EOR #O : Immediate
				op.c1 ^= reg.A;
				SET_NEGATIVE(op.c1); SET_ZERO(op.c1);
				reg.A = op.c1;
				INC(2); CYCLE(2);
				} break;
			case(EOR_ZPG) : {	// EOR O : Zero Page
				uint8 o = GET_ZP(op.c1) ^ reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(2); CYCLE(3);
				} break;
			case(EOR_ZPGX) : {	// EOR O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X) ^ reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(2); CYCLE(4);
				} break;
			case(EOR_ABS) : {	// EOR O : Absolute
				uint8 o = GET_MEM(op.s1) ^ reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(3); CYCLE(4);
				} break;
			case(EOR_ABSX) : {	// EOR O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X) ^ reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(3); CYCLE(4);
				} break;
			case(EOR_ABSY) : {	// EOR O, Y : Absolute
				uint8 o = GET_MEM(op.s1 + reg.Y) ^ reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(3); CYCLE(4);
				} break;
			case(EOR_INDX) : {	// EOR O, X : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.X)) ^ reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(2); CYCLE(6);
				} break;
			case(EOR_INDY) : {	// EOR O, Y : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.Y)) ^ reg.A;
				SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(2); CYCLE(5);
				} break;
			#pragma endregion
			#pragma region ASL // Shift A or mem left
			case(ASL_ACC) : {	// ASL : Accumulator
				SET_CARRY_7(reg.A);
				reg.A <<= 1;
				SET_NEGATIVE(reg.A); SET_ZERO(reg.A);
				INC(1); CYCLE(2);
				} break;
			case(ASL_ZPG) : {	// ASL O : Zero Page
				uint8 o = GET_ZP(op.c1);
				SET_CARRY_7(o);
				o <<= 1;
				SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, o);
				INC(2); CYCLE(5);
				} break;
			case(ASL_ZPGX) : {	// ASL O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X);
				SET_CARRY_7(o);
				o <<= 1;
				SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, o);
				INC(2); CYCLE(6);
				} break;
			case(ASL_ABS) : {	// ASL O : Absolute
				uint8 o = GET_MEM(op.s1);
				SET_CARRY_7(o);
				o <<= 1;
				SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, o);
				INC(3); CYCLE(6);
				} break;
			case(ASL_ABSX) : {	// ASL O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X);
				SET_CARRY_7(o);
				o <<= 1;
				SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, o);
				INC(3); CYCLE(7);
				} break;
			#pragma endregion
			#pragma region LSR // Shift A or mem right
			case(LSR_ACC) : {	// LSR : Accumulator
				SET_CARRY_0(reg.A);
				reg.A >>= 1;
				SET_NEGATIVE(reg.A); SET_ZERO(reg.A);
				INC(1); CYCLE(2);
				} break;
			case(LSR_ZPG) : {	// LSR O : Zero Page
				uint8 o = GET_ZP(op.c1);
				SET_CARRY_0(o);
				o >>= 1;
				SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, o);
				INC(2); CYCLE(5);
				} break;
			case(LSR_ZPGX) : {	// LSR O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X);
				SET_CARRY_0(o);
				o >>= 1;
				SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, o);
				INC(2); CYCLE(6);
				} break;
			case(LSR_ABS) : {	// LSR O : Absolute
				uint8 o = GET_MEM(op.s1);
				SET_CARRY_0(o);
				o >>= 1;
				SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, o);
				INC(3); CYCLE(6);
				} break;
			case(LSR_ABSX) : {	// LSR O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X);
				SET_CARRY_0(o);
				o >>= 1;
				SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, o);
				INC(3); CYCLE(7);
				} break;
			#pragma endregion
			#pragma region ROL // Rotate A or mem left
			case(ROL_ACC) : {	// ROL : Accumulator
				uint8 o = OP_ROL(reg.A);
				SET_CARRY_7(reg.A); SET_NEGATIVE(o); SET_ZERO(o);
				reg.A = o;
				INC(1); CYCLE(2);
				} break;
			case(ROL_ZPG) : {	// ROL O : Zero Page
				uint16 o = OP_ROL(GET_ZP(op.c1));
				SET_CARRY(o); SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, static_cast<uint8>(o));
				INC(2); CYCLE(5);
				} break;
			case(ROL_ZPGX) : {	// ROL O, X : Zero Page
				uint16 o = OP_ROL(GET_ZP(op.c1 + reg.X));
				SET_CARRY(o); SET_NEGATIVE(o); SET_ZERO(o);
				SET_ZP(op.c1, static_cast<uint8>(o));
				INC(2); CYCLE(6);
				} break;
			case(ROL_ABS) : {	// ROL O : Absolute
				uint16 o = OP_ROL(GET_MEM(op.s1));
				SET_CARRY(o); SET_NEGATIVE(o); SET_ZERO(o);
				SET_MEM(op.s1, static_cast<uint8>(o));
				INC(3); CYCLE(6);
				} break;
			case(ROL_ABSX) : {	// ROL O, X : Absolute
				uint16 o = OP_ROL(GET_MEM(op.s1 + reg.X));
				SET_CARRY(o); SET_NEGATIVE(o); SET_ZERO(o);
				SET_MEM(op.s1, static_cast<uint8>(o));
				INC(3); CYCLE(7);
				} break;
			#pragma endregion
			#pragma region ROR // Rotate A or mem right
			case(ROR_ACC) : {	// ROR : Accumulator
				uint8 o = OP_ROR(reg.A);
				SET_CARRY_0(reg.A); SET_NEGATIVE(0x00); SET_ZERO(o);
				reg.A = o;
				INC(1); CYCLE(2);
			} break;
			case(ROR_ZPG) : {	// ROR O : Zero Page
				uint16 o = OP_ROR_S(GET_ZP(op.c1));
				SET_CARRY(o); SET_NEGATIVE(0x00); SET_ZERO(o);
				SET_ZP(op.c1, static_cast<uint8>(o));
				INC(2); CYCLE(5);
			} break;
			case(ROR_ZPGX) : {	// ROR O, X : Zero Page
				uint16 o = OP_ROR_S(GET_ZP(op.c1 + reg.X));
				SET_CARRY(o); SET_NEGATIVE(0x00); SET_ZERO(o);
				SET_ZP(op.c1, static_cast<uint8>(o));
				INC(2); CYCLE(6);
			} break;
			case(ROR_ABS) : {	// ROR O : Absolute
				uint16 o = OP_ROR_S(GET_MEM(op.s1));
				SET_CARRY(o); SET_NEGATIVE(0x00); SET_ZERO(o);
				SET_MEM(op.s1, static_cast<uint8>(o));
				INC(3); CYCLE(6);
			} break;
			case(ROR_ABSX) : {	// ROR O, X : Absolute
				uint16 o = OP_ROR_S(GET_MEM(op.s1 + reg.X));
				SET_CARRY(o); SET_NEGATIVE(0x00); SET_ZERO(o);
				SET_MEM(op.s1, static_cast<uint8>(o));
				INC(3); CYCLE(7);
			} break;
#pragma endregion
			#pragma region BIT // Tests the first two bits of mem with A
			case(BIT_ZPG) : { // BIT O : Zero Page
				uint8 o = GET_ZP(op.c1);
				reg.P = ((o & 0x80) ? reg.P | FLAG_NEGATIVE : reg.P & (~FLAG_NEGATIVE)) |
						((o & 0x40) ? reg.P | FLAG_OVERFLOW : reg.P & (~FLAG_OVERFLOW));
				SET_ZERO(reg.A & o);
				INC(2); CYCLE(3);
			} break;
			case(BIT_ABS) : { // BIT O : Absolute
				uint8 o = GET_MEM(op.s1);
				reg.P = ((o & 0x80) ? reg.P | FLAG_NEGATIVE : reg.P & (~FLAG_NEGATIVE)) |
					((o & 0x40) ? reg.P | FLAG_OVERFLOW : reg.P & (~FLAG_OVERFLOW));
				SET_ZERO(reg.A & o);
				INC(3); CYCLE(4);
			} break;
			#pragma endregion
			#pragma endregion
			#pragma region Branch
			// All branch ops are RELATIVE
			case(BCC) : { // BCC : Branch on carry clear
				if (!GET_CARRY()) {
					reg.PC += static_cast<int8>(op.c1);
					CYCLE(3);	}
				else {
					INC(2);
					CYCLE(2);	}
			} break;
			case(BCS) : { // BCS : Branch on carry set
				if (GET_CARRY()) {
					reg.PC += static_cast<int8>(op.c1);
					CYCLE(3);	}
				else {
					INC(2);
					CYCLE(2);	}
			} break;
			case(BNE) : { // BNE : Branch on zero clear
				if (!GET_ZERO()) {
					reg.PC += static_cast<int8>(op.c1);
					CYCLE(3);	}
				else {
					INC(2);
					CYCLE(2);	}
			} break;
			case(BEQ) : { // BEQ : Branch on zero set
				if (GET_ZERO()) {
					reg.PC += static_cast<int8>(op.c1);
					CYCLE(3);	}
				else {
					INC(2);
					CYCLE(2);	}
			} break;
			case(BPL) : { // BPL : Branch on negative clear
				if (!GET_NEGATIVE()) {
					reg.PC += static_cast<int8>(op.c1);
					CYCLE(3);	}
				else {
					INC(2);
					CYCLE(2);	}
			} break;
			case(BMI) : { // BMI : Branch on negative set
				if (GET_NEGATIVE()) {
					reg.PC += static_cast<int8>(op.c1);
					CYCLE(3); 	}
				else {
					INC(2);
					CYCLE(2);	}
			} break;
			case(BVC) : { // BVC : Branch on overflow clear
				if (!GET_OVERFLOW()) {
					reg.PC += static_cast<int8>(op.c1);
					CYCLE(3); 	}
				else {
					INC(2);
					CYCLE(2);	}
			} break;
			case(BVS) : { // BVS : Branch on overflow set
				if (GET_OVERFLOW()) {
					reg.PC += static_cast<int8>(op.c1);
					CYCLE(3);	}
				else {
					INC(2);
					CYCLE(2);	}
			} break;
			#pragma endregion
			#pragma region Jump // Return
			case(JMP_ABS) : { // JMP : Absolute
				reg.PC = op.s1;
				CYCLE(3);
			} break;
			case(JMP_IND) : { // JMP : Indirect Absolute
				reg.PC = GET_MEM_S(op.s1);
				CYCLE(5);
			} break;
			case(JSR) : { // JSR : Absolute
				reg.S += 2;
				*reinterpret_cast<uint16*>(mem + 0x1FF - reg.S) = reg.PC + 0x03;
				reg.PC = op.s1;
				CYCLE(6);
			} break;
			case(RTI) :	// RTI
			case(RTS) : { // RTS
				reg.PC = *reinterpret_cast<uint16*>(mem + 0x1FF - reg.S);
				reg.S -= 2;
				CYCLE(6);
			} break;
			#pragma endregion
			#pragma region Registers
			case (CLC) : { // CLC : Implied
				reg.P &= (~FLAG_CARRY);
				INC(1); CYCLE(2);
			} break;
			case (CLD) : { // CLD : Implied
				reg.P &= (~FLAG_DECIMAL);
				INC(1); CYCLE(2);
			} break;
			case (CLI) : { // CLI : Implied
				reg.P &= (~FLAG_IRQ_DIS);
				INC(1); CYCLE(2);
			} break;
			case (CLV) : { // CLV : Implied
				reg.P &= (~FLAG_OVERFLOW);
				INC(1); CYCLE(2);
			} break;
			case (SEC) : { // SEC : Implied
				reg.P |= FLAG_CARRY;
				INC(1); CYCLE(2);
			} break;
			case (SED) : { // SED : Implied
				reg.P |= FLAG_DECIMAL;
				INC(1); CYCLE(2);
			} break;
			case (SEI) : { // SEI : Implied
				reg.P |= FLAG_IRQ_DIS;
				INC(1); CYCLE(2);
			} break;
			#pragma endregion
			#pragma region Compare
			#pragma region CMP // Compare to A
			case(CMP_IMM) : {	// CMP #O : Immediate
				OP_CMP(reg.A, op.c1);
				CYCLE(2); INC(2);
			} break;
			case(CMP_ZPG) : {	// CMP O : Zero Page
				uint8 o = GET_ZP(op.c1);
				OP_CMP(reg.A, o);
				CYCLE(3); INC(2);
			} break;
			case(CMP_ZPGX) : {	// CMP O, X : Zero Page
				uint8 o = GET_ZP(op.c1 + reg.X);
				OP_CMP(reg.A, o);
				CYCLE(4); INC(2);
			} break;
			case(CMP_ABS) : {	// CMP O : Absolute
				uint8 o = GET_MEM(op.s1);
				OP_CMP(reg.A, o);
				CYCLE(4); INC(3);
			} break;
			case(CMP_ABSX) : {	// CMP O, X : Absolute
				uint8 o = GET_MEM(op.s1 + reg.X);
				OP_CMP(reg.A, o);
				CYCLE(4); INC(3);
			} break;
			case(CMP_ABSY) : {	// CMP O, Y : Absolute
				uint8 o = GET_MEM(op.s1 + reg.Y);
				OP_CMP(reg.A, o);
				CYCLE(4); INC(3);
			} break;
			case(CMP_INDX) : {	// CMP O, X : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.X));
				OP_CMP(reg.A, o);
				CYCLE(6); INC(2);
			} break;
			case(CMP_INDY) : {	// CMP O, Y : Indirect
				uint8 o = GET_MEM(GET_MEM(op.c1 + reg.Y));
				OP_CMP(reg.A, o);
				CYCLE(5); INC(2);
			} break;
			#pragma endregion
			#pragma region CPX // Compare to X
			case(CPX_IMM) : {	// CMX #O : Immediate
				OP_CMP(reg.X, op.c1);
				CYCLE(2); INC(2);
			} break;
			case(CPX_ZP) : {	// CMX O : Zero Page
				uint8 o = GET_ZP(op.c1);
				OP_CMP(reg.X, o);
				CYCLE(3); INC(2);
			} break;
			case(CPX_ABS) : {	// CMX O : Absolute
				uint8 o = GET_MEM(op.s1);
				OP_CMP(reg.X, o);
				CYCLE(4); INC(3);
			} break;
			#pragma endregion
			#pragma region CPY // Compare to Y
			case(CPY_IMM) : {	// CMY #O : Immediate
				OP_CMP(reg.Y, op.c1);
				CYCLE(2); INC(2);
			} break;
			case(CPY_ZPG) : {	// CMY O : Zero Page
				uint8 o = GET_ZP(op.c1);
				OP_CMP(reg.Y, o);
				CYCLE(3); INC(2);
			} break;
			case(CPY_ABS) : {	// CMY O : Absolute
				uint8 o = GET_MEM(op.s1);
				OP_CMP(reg.Y, o);
				CYCLE(4); INC(3);
			} break;
			#pragma endregion
			#pragma endregion
			#pragma region Stack
			case(PHA) : { // PHA : Implied
				PUSH_STACK(reg.A);
				INC(1); CYCLE(3);
			} break;
			case(PHP) : { // PHP : Implied
				PUSH_STACK(reg.P);
				INC(1); CYCLE(3);
			} break;
			case(PLA) : { // PLA : Implied
				reg.A = PULL_STACK();
				INC(1); CYCLE(4);
			} break;
			case(PLP) : { // PLP : Implied
				reg.P = PULL_STACK();
				INC(1); CYCLE(4);
			} break;
			#pragma endregion

			}
		}

	}
}