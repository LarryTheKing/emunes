==============
    EmuNES
==============

******************************************************************************
	Unless otherwise stated, all data, code, etc. :
			Copyright (2014) Laurence T King
			
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
		
******************************************************************************

	The intention behind the creation of EmuNES was curiosity.
	I had originally wanted to create a n64 emulator but decided that the NES was a better starting point.

	The goals of this project include:
		
		To have a working interpreter of 6502 opcode

		To operate at a rate identical to the NES

		To be portable across systems

/////////////////////////////////////////////////////////////////////////////

	Currently all of the good stuff to be found is in Interpreter.cpp
		
		Be wary, the code makes heavy use of macros.
		
		I recommend having a 6502 reference handy.

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
