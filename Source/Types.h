#pragma once

#define INT8_MIN    (-0x7f - 1)
#define INT16_MIN   (-0x7fff - 1)
#define INT32_MIN   (-0x7fffffff - 1)
#define INT64_MIN   (-0x7fffffffffffffff - 1)

#define INT8_MAX    (0x7f)
#define INT16_MAX   (0x7fff)
#define INT32_MAX   (0x7fffffff)
#define INT64_MAX   (0x7fffffffffffffff)

#define UINT8_MAX   (0xff)
#define UINT16_MAX  (0xffff)
#define UINT32_MAX  (0xffffffff)
#define UINT64_MAX  (0xffffffffffffffff)

#ifdef _WIN64
#define INTX_MIN	INT64_MIN
#define INTX_MAX	INT64_MAX
#define UINTX_MIN	UINT64_MAX
#elif _WIN32
#define INTX_MIN	INT32_MIN
#define INTX_MAX	INT32_MAX
#define UINTX_MIN	UINT32_MAX
#endif


namespace EmuNES
{
	typedef				long	long	int64;
	typedef unsigned	long	long	uint64;
	typedef unsigned	long		ulong;
	typedef				int				int32;
	typedef unsigned	int				uint32;
	typedef unsigned	int			uint;
	typedef				short			int16;
	typedef unsigned	short			uint16;
	typedef	unsigned	short		ushort;
	typedef				char			int8;
	typedef	unsigned	char			uint8;
	typedef				uint8			bool8;

#ifdef _WIN64
	typedef int64						intx;
	typedef uint64						uintx;
#elif _WIN32
	typedef int32						intx;
	typedef uint32						uintx;
#endif

	typedef wchar_t					wchar;

}
